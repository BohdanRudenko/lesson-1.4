/*
    TASK 1

    Вывести в консоль с помощью цикла  WHILE, все нечетные числа от 30 до 40

*/
console.log('TASK 1');
let num = 30;
while(num<40){
  num++;
  if(num%2) {console.log(num)};
}
/*
    TASK 2

    Вывести в консоль с помощью цикла FOR, все четные числа от 70 до 40.
    Первым четным числом должно быть 70. 
    
*/
console.log('TASK 2');
for(let a = 70; a >=40; a--) {
    if( a % 2 == 0 ) {
        console.log(a);
    };
};
/*
    TASK 3

    Переведите цикл FOR из задания 2, в цикл WHILE.  
    
*/
console.log('TASK 3');
let b = 70;
while(b >= 40){
    if( b % 2 == 0 ) {
        console.log(b);
    };
    b--;
};
/*
    TASK 4

    С помощью любого известного Вам цикла из JavaScript, 
    найдите сумму чисел от 1 до 100, и выведите результат в консоль.   
    
*/
console.log('TASK 4');
let sum100 = 0, j = 1;
while(j<=100){
    sum100  += j;
    j++;
}
console.log(sum100);
/*
    TASK 5

    Вывести 10 строчек со смайликом : ':)'. 
    На первой строчке один смайлик, на второй два,и так далее. 
    На последней должно быть 10 смайликов.   
    
*/
console.log('TASK 5');
let oneSmile = ':)';
let smiles = '';
for(let i = 0; i < 10; i++) {
    smiles += oneSmile
    console.log(smiles)
};
/*
    TASK 6

    Напишите программу, которая выводит на экран числа от 1 до 50. 
    При этом вместо чисел, кратных трем, программа должна выводить слово «Java»,
    а вместо чисел, кратных пяти — слово «Script». 
    Если число кратно и 3, и 5, то программа должна выводить слово «JavaScript»   
    
*/
console.log('TASK 6');
for (let i = 0; i < 50; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
        console.log('JavaScript')
    }
    else if (i % 3 == 0) {
        console.log('Java')
    } 
    else if(i % 5 == 0) {
        console.log('Script')
    } 
    else {
        console.log(i)
    }
    
}
/* 
    TASK 7
    У нас есть число 1000
    Делите его на 2 столько раз, пока результат деления не станет меньше 50.
    Посчитайте количество итераций необходимых для выхода из цикла 
    и выведите результат в консоль

*/
console.log('TASK 7');
let result = 1000,
    attempt = 0;
 while (result > 50) {
    result = result / 2;
    attempt++;
    console.log(result);
 }
 console.log(attempt)

/*
    TASK 8
    Напишите программу, которая используя цикл while 
    выведет все числа от 45 до 170, кратные 10.

*/
console.log('TASK 8');
let y = 45;
while (y < 170) {
    y++;
    if (y % 10 == 0) {
        console.log(y)
    }
}
